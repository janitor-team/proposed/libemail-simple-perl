libemail-simple-perl (2.218-1) unstable; urgency=medium

  * Import upstream version 2.218.
  * Update years of packaging copyright.
  * Update upstream email address.
  * Declare compliance with Debian Policy 4.6.2.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.
  * Drop Breaks/Replaces on libemail-simple-creator-perl (removed 2010).

 -- gregor herrmann <gregoa@debian.org>  Sat, 14 Jan 2023 22:19:52 +0100

libemail-simple-perl (2.216-2) unstable; urgency=medium

  [ Laurent Baillet ]
  * fix lintian file-contains-trailing-whitespace warning

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Submit.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.1.5, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 04 Dec 2022 15:12:29 +0000

libemail-simple-perl (2.216-1) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Import upstream version 2.216.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.1.4.
  * Bump debhelper compatibility level to 10.

 -- gregor herrmann <gregoa@debian.org>  Fri, 22 Jun 2018 22:42:55 +0200

libemail-simple-perl (2.214-1) unstable; urgency=medium

  * Team upload

  [ Alex Muntada ]
  * Remove inactive pkg-perl members from Uploaders.

  [ Damyan Ivanov ]
  * New upstream version 2.214
  * declare conformance with Policy 4.1.1 (no changes needed)

 -- Damyan Ivanov <dmn@debian.org>  Wed, 25 Oct 2017 08:46:39 +0000

libemail-simple-perl (2.213-1) unstable; urgency=medium

  * Import upstream version 2.213.
  * Update years of packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Fri, 13 Jan 2017 22:09:31 +0100

libemail-simple-perl (2.211-1) unstable; urgency=medium

  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Brian Cassidy from Uploaders. Thanks for your work!
  * Remove Jeremiah Foster from Uploaders. Thanks for your work!
    Closes: #843740
  * Remove Jonathan Yu from Uploaders. Thanks for your work!
  * Remove Nathan Handler from Uploaders. Thanks for your work!

  * Import upstream version 2.211.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 3.9.8.
  * Drop build dependency already satisfied in src:perl in oldstable.

 -- gregor herrmann <gregoa@debian.org>  Tue, 27 Dec 2016 23:08:00 +0100

libemail-simple-perl (2.210-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ Lucas Kanashiro ]
  * Import upstream version 2.210
  * d/u/metadata: add email address from some contacts
  * Declare compliance with Debian policy 3.9.7

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Sun, 13 Mar 2016 22:47:16 -0300

libemail-simple-perl (2.208-1) unstable; urgency=medium

  * Team upload.
  * Add debian/upstream/metadata
  * Import upstream version 2.208
  * Bump debhelper compatibility level to 9

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Fri, 21 Aug 2015 15:15:48 -0300

libemail-simple-perl (2.206-1) unstable; urgency=medium

  * Team upload

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Nuno Carvalho ]
  * New upstream release
  * debian/control:
    + update standards version to 3.9.6
    + add "Testsuite: autopkgtest-pkg-perl"
  * debian/copyright: update upstream copyright information according to
    the COPYRIGHT sections in the provided modules

 -- Nuno Carvalho <smash@cpan.org>  Fri, 29 May 2015 19:15:37 +0100

libemail-simple-perl (2.203-1) unstable; urgency=low

  [ Florian Schlichting ]
  * Import Upstream version 2.202
  * Update Build-Dependencies

  [ gregor herrmann ]
  * New upstream release 2.203.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Sun, 16 Feb 2014 17:34:14 +0100

libemail-simple-perl (2.201-1) unstable; urgency=low

  * Team upload

  [ Nathan Handler ]
  * Email change: Nathan Handler -> nhandler@debian.org

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Florian Schlichting ]
  * Import Upstream version 2.201
  * Update Homepage to metacpan, the old one was unreachable since 2009
  * Bump dh compatibility to level 8 (no changes necessary)
  * Bump Standards-Version to 3.9.4 (no change)

 -- Florian Schlichting <fsfs@debian.org>  Tue, 20 Aug 2013 22:14:27 +0200

libemail-simple-perl (2.102-1) unstable; urgency=low

  * New upstream release
  * debian/control:
    - Add myself to list of Uploaders
    - Bump Standards-Version to 3.9.3
  * debian/copyright:
    - Update to copyright-format 1.0
    - Update my copyright years

 -- Nathan Handler <nhandler@ubuntu.com>  Tue, 17 Jul 2012 16:57:38 -0500

libemail-simple-perl (2.101-1) unstable; urgency=low

  * Team upload.
  * Imported Upstream version 2.101
    + Add 'use Email::Simple;' to SYNOPSIS of Email::Simple
      (Closes: #610691)
  * Update Format for debian/copyright.
    Update Format to revision 174 of DEP5 proposal for machine-readable
    copyright file information.

 -- Salvatore Bonaccorso <carnil@debian.org>  Sat, 24 Dec 2011 08:30:24 +0100

libemail-simple-perl (2.100-2) unstable; urgency=low

  [ gregor herrmann ]
  * Set Standards-Version to 3.9.1; replace Conflicts with Breaks.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.
  * debian/copyright: Refer to /usr/share/common-licenses/GPL-1; refer
    to "Debian systems" instead of "Debian GNU/Linux systems".
  * Update my email address.
  * Bump Standards-Version to 3.9.2.

  [ gregor herrmann ]
  * Remove transitional dummy package libemail-simple-creator-perl.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

 -- Ansgar Burchardt <ansgar@debian.org>  Thu, 15 Sep 2011 23:58:35 +0200

libemail-simple-perl (2.100-1) unstable; urgency=low

  [ Jonathan Yu ]
  * Refreshed copyright information
  * Add myself to Uploaders and Copyright
  * Update to new debhelper 7 rules format
  * Rewrite control description
  * Update dependencies per upstream
  * Add libtest-minimumversion-perl to B-D-I for testing
  * Add a dummy package because Email::Simple::Creator is now
    part of this package

  [ Nathan Handler ]
  * New upstream release
  * debian/watch: Update to ignore development releases.

  [ Brian Cassidy ]
  * New upstream release
  * debian/control: Added myself to Uploaders

  [ gregor herrmann ]
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).

  [ Salvatore Bonaccorso ]
  * debian/control: Changed: Replace versioned (build-)dependency on
    perl (>= 5.6.0-{12,16}) with an unversioned dependency on perl (as
    permitted by Debian Policy 3.8.3).

 -- Jonathan Yu <jawnsy@cpan.org>  Sun, 15 Nov 2009 13:04:07 -0500

libemail-simple-perl (2.004-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * New upstream release.
  * Add myself to uploaders.
  * Bump Standards Version to 3.8.0 (no changes required)

  [ gregor herrmann ]
  * debian/copyright: use dist-based upstream source URL.
  * debian/control: change my email address.
  * Refresh debian/rules, no functional changes.

 -- Ansgar Burchardt <ansgar@43-1.org>  Sun, 29 Jun 2008 21:43:27 +0200

libemail-simple-perl (2.003-2) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Removed:
    Homepage pseudo-field (Description); XS-Vcs-Svn fields.
  * debian/rules: delete /usr/lib/perl5 only if it exists.

  [ Rene Mayorga ]
  * Make package ready for Perl 5.10 update
  * debian/control
    + wrap uploaders field
    + set debhelper version to 6
    + remove perl-modules from B-D-I
    + Set standards-version to 3.7.3 ( no changes needed )
  * debian/watch improved
  * debian/rules
    + use dh_listpackages instead of hardcode package name
    + remove OPTIMIZE var, we are not compiling anything
    + move tests suites from install target to build target
    + use $@ when touching -stamp files
    + remove un-ussed dh_calls
    + Add install-stamp target and make install depends on it

 -- Rene Mayorga <rmayorga@debian.org.sv>  Sun, 27 Jan 2008 23:09:10 -0600

libemail-simple-perl (2.003-1) unstable; urgency=low

  * New upstream release
  * Added myself to uploaders

 -- Jeremiah Foster <jeremiah@jeremiahfoster.com>  Tue, 14 Aug 2007 16:34:41 +0000

libemail-simple-perl (2.002-1) unstable; urgency=low

  * New upstream release.
  * Update debian/copyright.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Mon, 16 Jul 2007 17:08:21 +0200

libemail-simple-perl (1.999-1) unstable; urgency=low

  * New upstream release.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Sat, 14 Apr 2007 00:56:15 +0200

libemail-simple-perl (1.996-1) unstable; urgency=low

  * New upstream release

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Wed, 29 Nov 2006 10:15:19 +0100

libemail-simple-perl (1.995-1) unstable; urgency=low

  * New upstream release.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Sun, 19 Nov 2006 20:05:40 +0100

libemail-simple-perl (1.992-1) unstable; urgency=low

  * New upstream release.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Fri,  6 Oct 2006 22:13:26 +0200

libemail-simple-perl (1.990-1) unstable; urgency=low

  * New upstream release.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Sat,  9 Sep 2006 20:56:47 +0200

libemail-simple-perl (1.980-1) unstable; urgency=low

  * New upstream release

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Thu, 31 Aug 2006 15:27:14 +0200

libemail-simple-perl (1.96-1) unstable; urgency=low

  * New upstream release

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Thu,  3 Aug 2006 16:09:45 +0200

libemail-simple-perl (1.95-1) unstable; urgency=low

  * New upstream release.
  * Add upstream homepage URL to the long description.
  * Depend on ${misc:Depends}, as recommended by debhelper.
  * Don't ignore the return value of 'make realclean'.
  * Don't install the upstream README, as it's just the module manpage.

 -- Niko Tyni <ntyni@iki.fi>  Thu, 27 Jul 2006 23:32:06 +0300

libemail-simple-perl (1.94-1) unstable; urgency=low

  * New upstream release.
  * Add libtest-pod-perl and libtest-pod-coverage-perl to Build-Depends-Indep.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Fri, 14 Jul 2006 22:08:12 +0200

libemail-simple-perl (1.92-2) unstable; urgency=low

  * Moved debhelper to Build-Depends.
  * Set Standards-Version to 3.7.2 (no changes).
  * Set Debhelper Compatibility Level to 5.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Fri, 16 Jun 2006 15:37:34 +0200

libemail-simple-perl (1.92-1) unstable; urgency=low

  * Taking orphaned package for Debian Perl Group (closes: #357343)
  * Confirmed fixing of NMU bugs (closes #297920), (closes: #329533),
    (closes: #338135)

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Fri, 17 Mar 2006 12:19:15 +0100

libemail-simple-perl (1.92-0.1) unstable; urgency=low

  * Non-maintainer upload.
  * New upstream release (Closes: #329533)
  * Add watch file
  * Bump Standards-Version (no changes)
  * Fix copyright file (Closes: #338135)
  * Fix handling of header casing (patch from Kai Henningsen) (Closes: #297920)
  * Switch to debhelper compatability level 4

 -- Paul Wise <pabs3@bonedaddy.net>  Thu, 15 Dec 2005 10:56:24 +0800

libemail-simple-perl (1.9-1) unstable; urgency=medium

  * Initial Release. (Closes:Bug#194546)

 -- S. Zachariah Sprackett <zac@sprackett.com>  Thu, 15 Jul 2004 20:38:14 -0400
